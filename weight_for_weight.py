import unittest

def order_weight(string):
    weight_list = [sum([int(i) for i in digit]) for digit in string.split()]
    ordered = sorted(zip(weight_list,string.split()))
    while True:
        change = False
        for i in range(len(ordered)-1):
            if (ordered[i][0] == ordered[i+1][0] and 
                    string.index(ordered[i][1]) > string.index(ordered[i][1])):  
                temp1, temp2 = ordered[i], ordered[i+1]
                ordered[i],ordered[i+1] = temp2, temp1
                change = True
        if change == False:
            break     
    return " ".join([i[1] for i in ordered])

class TestOrderWeight(unittest.TestCase):
    def test_equal(self):
        self.assertEqual(order_weight("103 123 4444 99 2000"), "2000 103 123 4444 99")
        self.assertEqual(order_weight("2000 10003 1234000 44444444 9999 11 11 22 123"), "11 11 2000 10003 22 123 1234000 44444444 9999")

if __name__ == '__main__':
    unittest.main()

