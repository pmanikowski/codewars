import unittest


def power(s):
    output = [s]
    copy_s = [s] 
    def inner_power(l):
        inner_output = []
        unique = []
        for sublist in l:
            for z in sublist:
                inner_output.append([j for j in sublist if j != z])
        [unique.append(item) for item in inner_output if item not in unique]
        return unique            
    for i in range(len(s)):
        copy_s = inner_power(copy_s)
        output += copy_s                        
    return output


class TestDigit(unittest.TestCase):


    def test_set1(self):
        self.assertEqual(power([1,2,3,4,5,6]), 
[[1, 2, 3, 4, 5, 6], [2, 3, 4, 5, 6], [1, 3, 4, 5, 6], [1, 2, 4, 5, 6],
 [1, 2, 3, 5, 6], [1, 2, 3, 4, 6], [1, 2, 3, 4, 5], [3, 4, 5, 6], 
[2, 4, 5, 6], [2, 3, 5, 6], [2, 3, 4, 6], [2, 3, 4, 5], [1, 4, 5, 6], 
[1, 3, 5, 6], [1, 3, 4, 6], [1, 3, 4, 5], [1, 2, 5, 6], [1, 2, 4, 6], 
[1, 2, 4, 5], [1, 2, 3, 6], [1, 2, 3, 5], [1, 2, 3, 4], [4, 5, 6], 
[3, 5, 6], [3, 4, 6], [3, 4, 5], [2, 5, 6], [2, 4, 6], [2, 4, 5], 
[2, 3, 6], [2, 3, 5], [2, 3, 4], [1, 5, 6], [1, 4, 6], [1, 4, 5], 
[1, 3, 6], [1, 3, 5], [1, 3, 4], [1, 2, 6], [1, 2, 5], [1, 2, 4], 
[1, 2, 3], [5, 6], [4, 6], [4, 5], [3, 6], [3, 5], [3, 4], [2, 6], 
[2, 5], [2, 4], [2, 3], [1, 6], [1, 5], [1, 4], [1, 3], [1, 2], 
[6], [5], [4], [3], [2], [1], []])

    def test_set2(self):
        self.assertEqual(power([1,2,3,4,]), [[1, 2, 3, 4], [2, 3, 4], 
[1, 3, 4], [1, 2, 4], [1, 2, 3], [3, 4], [2, 4], [2, 3], [1, 4], [1, 3], 
[1, 2], [4], [3], [2], [1], []])

    def test_set3(self):
        self.assertEqual(power([1,2,3]), [[1, 2, 3], [2, 3], [1, 3], 
[1, 2], [3], [2], [1], []])

    def test_set4(self):
        self.assertEqual(power([1,2]), [[1, 2], [2], [1], []])

if __name__ == '__main__':
    unittest.main()




