import unittest


def calc(expr):    
    if expr == '':
        return 0
    expr_split = expr.split()
    operation_indexes = []
    counter = 0
    for i in expr_split:
        if i in ['+', '-', '*', '/']:
            operation_indexes.append(counter)
        counter+=1         
    digits = [float(i) for i in expr_split if i not in ['+', '-', '*', '/']]
    if operation_indexes == []:
        return digits[-1]
    k = 0
    while operation_indexes:
        operation = operation_indexes[0]
        a = digits[operation-2]
        b = digits[operation-1]
        if expr_split[operation+2*k] == '+':
            digits[operation-2:operation] = [a+b]   
        elif expr_split[operation+2*k] == '-':
            digits[operation-2:operation] = [a-b]
        elif expr_split[operation+2*k] == '*':
            digits[operation-2:operation] = [a*b]
        elif expr_split[operation+2*k]  == '/':
            digits[operation-2:operation] = [a/b]
        operation_indexes = [o-2 for o in operation_indexes[1:]]
        k += 1
    return digits[0] 


class TestPolish(unittest.TestCase):
    def test_simple(self):
        self.assertEqual(calc(""), 0)
        self.assertEqual(calc("1 2 3"), 3)
        self.assertEqual(calc("1 2 3.5"), 3.5)

    def test_operations(self):
        self.assertEqual(calc("1 3 +"), 4)
        self.assertEqual(calc("1 3 *"), 3)
        self.assertEqual(calc("1 3 -"), -2)
        self.assertEqual(calc("5 2.5 /"), 2)

    def test_complex(self):
        self.assertEqual(calc("5 1 2 + 4 * + 3 -"), 14)
    
if __name__ == '__main__':
    unittest.main()

