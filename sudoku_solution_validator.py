import unittest


def validSolution(board):
    flat_board = [item for sublist in board for item in sublist]
    for i in range(9):
        if set ([j for j in flat_board[i:i+73:9]]) != set(range(1,10)):
            return False        
        elif set(flat_board[i*9:(i+1)*9]) != set(range(1,10)): 
            return False
    for i in range(0,8,3):
        for j in range(2,9,3):
            box = []
            for k in range(3):
                box.append(board[i+k][j-2:j+1])
            if set([item for sublist in box for item in sublist]) != set(range(1,10)):
                return False
    return True


class TestSudoku(unittest.TestCase):
    def test_valid(self):
        self.assertTrue(validSolution([[5, 3, 4, 6, 7, 8, 9, 1, 2], 
                         [6, 7, 2, 1, 9, 5, 3, 4, 8],
                         [1, 9, 8, 3, 4, 2, 5, 6, 7],
                         [8, 5, 9, 7, 6, 1, 4, 2, 3],
                         [4, 2, 6, 8, 5, 3, 7, 9, 1],
                         [7, 1, 3, 9, 2, 4, 8, 5, 6],
                         [9, 6, 1, 5, 3, 7, 2, 8, 4],
                         [2, 8, 7, 4, 1, 9, 6, 3, 5],
                         [3, 4, 5, 2, 8, 6, 1, 7, 9]])) 

    def test_invalid(self):
        self.assertFalse(validSolution([[5, 3, 4, 6, 7, 8, 9, 1, 2], 
                         [6, 7, 2, 1, 9, 0, 3, 4, 9],
                         [1, 0, 0, 3, 4, 2, 5, 6, 0],
                         [8, 5, 9, 7, 6, 1, 0, 2, 0],
                         [4, 2, 6, 8, 5, 3, 7, 9, 1],
                         [7, 1, 3, 9, 2, 4, 8, 5, 6],
                         [9, 0, 1, 5, 3, 7, 2, 1, 4],
                         [2, 8, 7, 4, 1, 9, 6, 3, 5],
                         [3, 0, 0, 4, 8, 1, 1, 7, 9]]))           

if __name__ == '__main__':
    unittest.main()

