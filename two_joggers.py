import unittest

def nbr_of_laps(x, y):
    intersection = min(set(range(x, x*y+1, x)).intersection(set(range(y, x*y+1, y))))
    return [intersection/x, intersection/y]


class TestGcd(unittest.TestCase):


    def test_simple(self):
        self.assertEqual(nbr_of_laps(12, 14), [7, 6])
        self.assertEqual(nbr_of_laps(13, 17), [17, 13])
        self.assertEqual(nbr_of_laps(5, 6), [6, 5])
    
if __name__ == '__main__':
    unittest.main()
