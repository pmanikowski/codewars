from math import pi, log, floor, ceil
import unittest


def converter(n, decimals=0, base=pi):
    """takes n in base 10 and returns it in any base (default is pi
    with optional x decimals"""
    conversion_base = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    output = []


    if n == 0:
        output = ['0', '.'] + ['0' for i in range(decimals)]
        return "".join(output)
    elif n < 0:
        n = abs(n)
        output = ['-']
    temp_n = n
    for exponent in range(int(floor(log(n, base))), -decimals-1, -1):
        if exponent == -1:
            output.append('.')        
        for i in range(1, int(ceil(base + 0.0001))):
            temp_n -= i * base**exponent
            if temp_n < 0:
                output.append(conversion_base[i-1])
                n -= (i - 1) * base**exponent
                break 
            elif i != int(floor(base)):
                temp_n = n        
        if temp_n > 0:
            output.append(conversion_base[int(floor(base - 0.0001))])
            n = temp_n
        else:
            temp_n = n        
    return "".join(output)


class TestConverted(unittest.TestCase):


    def test_simplest(self):
        self.assertEquals(converter(13), '103')
        self.assertEquals(converter(10), '100')
        self.assertEquals(converter(pi), '10')
    
    def test_with_decimals(self):
        self.assertEquals(converter(13,3), '103.010')
        self.assertEquals(converter(10,decimals=2), '100.01')
        self.assertEquals(converter(pi, 5), '10.00000')
    
    def test_other_bases(self):
        self.assertEquals(converter(13,0,8), '15')
        self.assertEquals(converter(10, 0, 16), 'A')
        self.assertEquals(converter(pi, base=2), '11')
        self.assertEquals(converter(7,0,19), '7')
        self.assertEquals(converter(1,base=2), '1')

    def test_decimal(self):
        self.assertEquals(converter(13.5,4,16), 'D.8000')
        self.assertEquals(converter(10.5,0,16), 'A')
        self.assertEquals(converter(1,2,2), '1.00')

    def test_negative(self):
        self.assertEquals(converter(-10,0,23), '-A')
        self.assertEquals(converter(0,4,26), '0.0000')
        self.assertEquals(converter(-15.5,2,23), '-F.BB') 

    def test_ten_base(self):
        self.assertEquals(converter(13,0,10), '13')
        self.assertEquals(converter(10,base=10), '10')
        self.assertEquals(converter(5.5,1,10), '5.5')

if __name__ == '__main__':
    unittest.main()

