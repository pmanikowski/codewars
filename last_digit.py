import unittest

def last_digit(n1,n2):   
    return [int(str((n1%10)**i)[-1]) for i in range(1,5)][n2%4-1]


class TestDigit(unittest.TestCase):
    def test_set1(self):
        self.assertEqual(last_digit(4, 1), 4)
        self.assertEqual(last_digit(4, 2), 6)
        self.assertEqual(last_digit(9, 7), 9)

    def test_set2(self):
        self.assertEqual(last_digit(10, 10 ** 10), 0)
        self.assertEqual(last_digit(2 ** 200, 2 ** 300), 6)

if __name__ == '__main__':
    unittest.main()

