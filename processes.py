from random import choice
import unittest

def connections(process_chain, node):
    node_connections = []
    for process in process_chain:
        if process[1] == node: 
            node_connections.append(process)    
    return node_connections

def processes(start, end, process_chain):
    path = []
    path_length = 0
    if start == end:
        return []
    if end not in [process[2] for process in process_chain]:
        return []    
    j = 0
    max_length = len(process_chain)+1
    while j < 100:
        temp_path = []
        current_node = start
        path_length = 0
        while True:            
            outpath = connections(process_chain, current_node)
            if outpath == []:
                break      
            next_node = choice(outpath)
            temp_path.append(next_node[0])
            path_length += 1
            current_node = next_node[2]
            if path_length >= max_length:
                break
            if current_node == end and path_length < max_length:
                path = temp_path
                max_length = path_length
                break        
        j += 1

    return path

test_processes = [
    ['gather', 'field', 'wheat'],
    ['bake', 'flour', 'bread'],
    ['mill', 'wheat', 'flour']
];
test_processes2 = [
    ['a', '1', '2'],
    ['b', '2', '5'],
    ['c', '1', '7'],
    ['d', '4', '7'],
    ['e', '7', '8'],
    ['f', '7', '11'],
    ['g', '5', '9'],
    ['h', '9', '10'],
    ['i', '8', '9'],
    ['j', '11', '6'],
    ['k', '10', '6'],
    ['l', '2', '7'],
];


class TestProcesses(unittest.TestCase):
    def test_set1(self):
        self.assertEqual(processes('field', 'bread', test_processes), ['gather', 'mill', 'bake'])
        self.assertEqual(processes('field', 'ferrari', test_processes), [])
        self.assertEqual(processes('field', 'field', test_processes), [])

    def test_set2(self):
        self.assertEqual(processes('1', '6', test_processes2), ['c', 'f', 'j'])
        self.assertEqual(processes('2', '8', test_processes2), ['l', 'e'])
        self.assertEqual(processes('5', '11', test_processes2), [])

if __name__ == '__main__':
    unittest.main()





