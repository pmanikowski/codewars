import unittest


def check_next(subfield):
    ship_length = 1


    for pool in subfield:
        if pool != 1: 
            break
        ship_length += 1        
    return ship_length      


def validateBattlefield(field):
    ships_container = {1:0, 2:0, 3:0, 4:0}
    transpose = list(map(list, zip(*field)))
    row_index = 0


    for row in field:
        column_index = 0
        for column in row:
            if column == 1:
                h_length = check_next(field[row_index][column_index+1:column_index+5])     
                v_length = check_next(transpose[column_index][row_index+1:row_index+5])
                ship_size = max(h_length, v_length) 
                if h_length > v_length:
                    neighbourhood = (field[row_index+1][column_index-1:column_index+ship_size+1] +
                                             [field[row_index][column_index+ship_size]])
                else: 
                    neighbourhood = (transpose[column_index+1][row_index-1:row_index+ship_size+1] + 
                                   [transpose[column_index][row_index+ship_size]])
                if sum(neighbourhood) != 0:
                    return False                
                ships_container[ship_size] += 1
            column_index += 1
        row_index += 1               
    return ships_container == {1:10, 2:6, 3:3, 4:1}

battleField = [[1, 0, 0, 0, 0, 1, 1, 0, 0, 0],
               [1, 0, 1, 0, 0, 0, 0, 0, 1, 0],
               [1, 0, 1, 0, 1, 1, 1, 0, 1, 0],
               [1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
               [0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
               [0, 0, 0, 0, 1, 1, 1, 0, 0, 0],
               [0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
               [0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
               [0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
               [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]

battleField2 = [[1, 0, 0, 0, 0, 1, 1, 0, 0, 0],
               [1, 0, 1, 0, 0, 0, 0, 0, 1, 0],
               [1, 0, 1, 0, 1, 1, 1, 0, 1, 0],
               [1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
               [0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
               [0, 0, 0, 0, 1, 1, 1, 0, 0, 0],
               [0, 0, 0, 1, 0, 0, 0, 0, 1, 0],
               [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
               [0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
               [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]



class TestBattleship(unittest.TestCase):


    def test_simple(self):
        self.assertTrue(validateBattlefield(battleField))
        self.assertFalse(validateBattlefield(battleField2))

if __name__ == '__main__':
    unittest.main()
